import socket
import datetime

HOST = '34.218.16.79'
PORT = 77


def get_date(add_days=0):
    """
    take the current date and change the format of the date from: d-m-y to m/d/y
    and adds the amount of days requested by the user
    :param add_days: amount of days to add to get (ex. 2 days from today) in int
    :return: the formated date in string
    """
    days_to_add = datetime.timedelta(add_days)
    curr_date = datetime.date.today() + days_to_add

    # take curr_date (format: y-m-d) and convert to neaded format (m/d/y)
    curr_date = datetime.datetime.strptime(str(curr_date), '%Y-%m-%d')  # make formatable
    return curr_date.strftime('%d/%m/%Y')  # format


def calc_checksum(city, date):
    """
    calculates the checksum of the city and
    the date to send to the weather client
    :param city: city name in str
    :param date: date in str
    :return: the checksum in str
    """
    city_check = 0
    date_check = 0
    abc = 'abcdefghijklmnopqrstuvwxyz'

    for letter in city.lower():  # add placement in abc
        city_check += abc.find(letter) + 1

    for num in date:
        if num.isdigit():
            date_check += int(num)

    checksum = str(city_check) + '.' + str(date_check)
    return checksum


def extract_temp(response):
    """
    extracts the tempeture and the weather description from a response from the weather client
    also checks if it is a valid response
    :param response: as string
    :return: tuple of tempeture and the weather description
    """
    if 'ERROR' in response:
        temp = 999
        descript = response.split(':')[2]
    else:
        res = response.split('&')
        temp = float(res[2][5:])
        descript = res[3][5:]
    
    return temp, descript


def get_weather(city_name, date):
    """
    gets the tempeture and the description of the day in a certain city in a
    certain date, using the weather client server (connect true socket)
    :param city_name: the name of the city and country (format: city, country) as string
    :param date: date to check (format: xx/xx/xxxx) as string
    :return: tuple with the deired tempeture and description of the weather (ex. (12.5, 'raining'))
    """
    city = city_name.split(',')[0]
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect((HOST, PORT))
        checksum = calc_checksum(city_name, date)

        send_data = f"100:REQUEST:city={city_name}&date={date}&checksum={checksum}"
        sock.recv(1024)
        sock.sendall(send_data.encode())

        response = sock.recv(1024).decode()

        return extract_temp(response)


def main():
    """
    weather client to the weather server, make a choice to get the current weather
    in a city, or the weather for today and the next 3 days.
    :return: none
    """
    print('Welcome to the weather client!')
    print("choose an option(1/2):\n1) what is the weather today\n2) what is the weather in the next 3 days")
    choice = int(input("--> "))
    city = input('Enter the city you live in (city, country): ')

    if choice == 1:
        curr_date = get_date()
        print(get_weather(city, curr_date))

    elif choice == 2:
        for i in range(4):
            curr_date = get_date(i)
            weather = get_weather(city, curr_date)
            print(f"{curr_date}, Temperature: {weather[0]}, {weather[1]}.")


if __name__ == '__main__':
    main()
