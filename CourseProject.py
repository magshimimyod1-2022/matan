import termcolor as tm


def remove_duplicates(item_list):
    """
    removes the duplicates from the list
    :param item_list: the list with dupes
    :return: the list without dupes
    """
    times = 0

    for i in item_list:
        for j in item_list:
            if j == i and (item_list.index(j) != times):
                item_list.remove(j)
        times += 1

    return item_list


def choose_word(file_path, index):
    """
    get a word from the list of words in a file and find out how much
    different words are in list
    :param file_path: the path to the file with the word list
    :param index: the index if the word to get from file
    :return: tuple with length of words list and the wanted word
    """
    # open file and get contents
    word_list = open(file_path, 'r').read()
    word_list = word_list.split(' ')
    # get size without duplicates
    list_len_no_dupes = len(remove_duplicates(word_list.copy()))
    # get the word from index, even if index is larger than word size
    while index > len(word_list):
        index -= len(word_list)
    secret_word = word_list[index - 1]

    return list_len_no_dupes, secret_word


def print_hangman(num_of_tries):
    """
    prints the coresponding hangman picture to the number of tries
    :param num_of_tries: the number of tries left
    :return: none
    """
    HANGMAN_PHOTOS = {1: """    x-------x""", 2: """    x-------x
    |
    |
    |
    |
    |""", 3: """    x-------x
    |       |
    |       0
    |
    |
    |""", 4: """    x-------x
    |       |
    |       0
    |       |
    |
    |""", 5: """    x-------x
    |       |
    |       0
    |      /|\ 
    |
    |""", 6: """    x-------x
    |       |
    |       0
    |      /|\ 
    |      /
    |""", 7: """    x-------x
    |       |
    |       0
    |      /|\ 
    |      / \ 
    |"""}
    print(HANGMAN_PHOTOS[num_of_tries])


def check_valid_input(letter_guessed, old_letters_guessed):
    """
    checks if a letter guessed is in the alfabet and is only one char long and not in the old list
    :param letter_guessed: the letter that the user guessed
    :param old_letters_guessed: a list of the letters that are already used
    :return: true if it is valid false if not
    """

    letter_guessed = letter_guessed.lower()
    # if guess is more than 1 letter or not alfabet or in list
    if len(letter_guessed) > 1 or not (letter_guessed.isalpha()) or (letter_guessed in old_letters_guessed):
        is_valid = False
    else:  # if it is good
        is_valid = True

    return is_valid


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
    trys to update the letter guessed to the list if it is valid
    if not it prints the list in order
    :param letter_guessed: the letter that the user guessed
    :param old_letters_guessed: a list of the letters that are already used
    :return: true if is added false if not
    """
    letter_guessed = letter_guessed.lower()
    is_valid = check_valid_input(letter_guessed, old_letters_guessed)

    if is_valid:
        old_letters_guessed += letter_guessed
        return True
    else:
        print('X')
        print(' -> '.join(sorted(old_letters_guessed)))  # make string with old letters
        return False


def show_hidden_word(secret_word, old_letters_guessed):
    """
    prints '_ _ _' according to the letters guest
    :param secret_word: string
    :param old_letters_guessed: list of letters
    :return: string with the underlines
    """
    hidden_word = ''
    i = 0
    for letter in secret_word:
        if letter in old_letters_guessed:
            hidden_word += letter
        else:
            hidden_word += '_'
        if i != len(secret_word) - 1:  # if not last letter
            hidden_word += ' '
        i += 1
    return hidden_word


def check_win(secret_word, old_letters_guessed):
    """
    check with the secret word if the user won the game (all letters guest)
    :param secret_word: str
    :param old_letters_guessed: list of letters
    :return: true if won, false if not
    """
    secret_word = show_hidden_word(secret_word, old_letters_guessed)
    if not ('_' in secret_word):
        return True
    else:
        return False


def open_screen(max_tries):
    """
    print the opening screen
    :param max_tries:
    :return:
    """
    HANGMAN_ASCII_ART = """      _    _                                         
     | |  | |                                        
     | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __  
     |  __  |/ _` | '_ \ / _` | '_ ` _ \ / _` | '_ \ 
     | |  | | (_| | | | | (_| | | | | | | (_| | | | |
     |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                          __/ |                      
                         |___/
    """
    tm.cprint(HANGMAN_ASCII_ART, 'blue')
    tm.cprint(max_tries, 'blue')


def main():
    """
    play the hangman game, enter file path, and index of letter to play the game
    how to play: guess a letter you have 6 wrong guesses to find the word
    """
    MAX_TRIES = 6
    open_screen(MAX_TRIES)

    file_path = input('Enter the file path: ')
    index = int(input('Enter an index for the word: '))
    secret_word = choose_word(file_path, index)[1]
    print('\nlets start!')

    old_letters_guest = []
    num_of_tries = 1
    print_hangman(num_of_tries)
    print(show_hidden_word(secret_word, old_letters_guest))

    update = False
    win = False
    user_guess = ''

    while not win and num_of_tries <= MAX_TRIES:
        # get user input and check validation
        while not update:
            user_guess = input("Guess a letter: ")
            update = try_update_letter_guessed(user_guess, old_letters_guest)
        # print underline and check if win
        print(show_hidden_word(secret_word, old_letters_guest))
        win = check_win(secret_word, old_letters_guest)
        # if write guess and if not
        if not win and user_guess not in secret_word:
            num_of_tries += 1
            print_hangman(num_of_tries)
        elif win:
            print('WIN')
        # update variables
        update = False

    if not win:
        print(show_hidden_word(secret_word, old_letters_guest))
        print('LOSE')


if __name__ == '__main__':
    main()
